Information Gathering

General:
Gathering general information about the target such as: Executives, employees, investors, vendors, locations, goals of the business, etc.

Infrastructure:
Gather info on the IP addresses, domains, DNS info, servers, operating systems, Websites, and web applications, etc.

